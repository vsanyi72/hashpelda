package vs.hash;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Start {

	public static void main(String[] args) {
		
		try {
			
			String input = "ABC";
			MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
			byte[] result = messageDigest.digest(input.getBytes(StandardCharsets.UTF_8));
			

			StringBuilder sb = new StringBuilder();
			
			for (int i = 0; i < result.length; i++) {
				sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
			}
			
//			 String hex = "";
//			      for (byte i : result) {
//		            hex += String.format("%02X", i);
//		        }
//		         System.out.print(hex);
			
			String sha512Dig = sb.toString();
			
			System.out.println("Bemenet : " + input);
			System.out.println("Kimenet : " + sha512Dig);
			System.out.println("Kimenet hossz : " + sha512Dig.length() ) ;
			
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
